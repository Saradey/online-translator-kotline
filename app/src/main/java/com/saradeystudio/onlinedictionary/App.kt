package com.saradeystudio.onlinedictionary

import com.saradeystudio.onlinedictionary.di.component.ApplicationComponent
import com.saradeystudio.onlinedictionary.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App : DaggerApplication(){

    companion object {
        lateinit var component : ApplicationComponent
    }

    override fun applicationInjector():
            AndroidInjector<out DaggerApplication> = component



    override fun onCreate() {
        component = DaggerApplicationComponent
            .builder()
            .build()

        super.onCreate()
    }



}